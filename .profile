if [[ -n $BASH ]]; then
    set -a
    for f in ~/.config/environment.d/*.conf; do
        source $f
    done
    set +a
fi

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [[ -z $WAYLAND_DISPLAY ]] && [[ $(tty) = "/dev/tty1" ]]; then
    export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/gcr/ssh"
    exec sway
fi
