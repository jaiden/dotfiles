# ~/.bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -r "$CARGO_HOME/env" ] && . "$CARGO_HOME/env"
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
[ -r /usr/share/clang/bash-autocomplete.sh ] && . /usr/share/clang/bash-autocomplete.sh

set -o vi
stty -ixon

shopt -s autocd
shopt -s globstar
shopt -s checkwinsize

alias x509inspect='openssl x509 -text -noout -in'

qinfo() {
    curl https://cht.sh/$1
}

weather() {
    curl wttr.in/$1
}

__git_ps1='git rev-parse --abbrev-ref HEAD 2>/dev/null||printf ""'

export PS1="\[\033[38;5;98m\]\h\[$(tput sgr0)\]\[\033[38;5;15m\]@\[$(tput sgr0)\]\[\033[38;5;206m\]\u\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]\[\033[38;5;228m\]\A\[$(tput sgr0)\]\[\033[38;5;15m\] [\[$(tput sgr0)\]\[\033[38;5;156m\]\W\[$(tput sgr0)\] \$(${__git_ps1})\[\033[38;5;15m\]]\n\[$(tput sgr0)\]\[\033[38;5;195m\]\\$\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

eval "$(direnv hook bash)"
